<?php
/**
 * @file
 * Admin view functions
 */

/**
 * Admin page template info.
 *
 * @return array
 *   Template structure
 */
function sphinxdrupal_admin() {
  $form = array();

  $form['sphinxdrupal_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Host name'),
    '#default_value' => variable_get('sphinxdrupal_host', 'localhost'),
    '#size' => 50,
    '#description' => t("The host name where Sphinx is running."),
    '#required' => TRUE,
  );

  $form['sphinxdrupal_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Port number'),
    '#default_value' => variable_get('sphinxdrupal_port', '3312'),
    '#size' => 8,
    '#maxlength' => 8,
    '#description' => t("The port number which listened by Sphinx."),
    '#required' => TRUE,
  );

  $form['sphinxdrupal_index'] = array(
    '#type' => 'textfield',
    '#title' => t('Index name'),
    '#default_value' => variable_get('sphinxdrupal_index', 'drupal_search'),
    '#size' => 50,
    '#description' => t("The Sphinx index name declared in sphinx.conf."),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Admin page validation.
 *
 * @param string $form
 *   Name of the form
 * @param array $form_state
 *   State of the form
 */
function sphinxdrupal_admin_validate($form, &$form_state) {
  $port = $form_state['values']['sphinxdrupal_port'];
  if (!is_numeric($port)) {
    form_set_error('sphinxdrupal_port', t('You must enter an integer for the port number.'));
  }
  elseif ($port <= 0) {
    form_set_error('sphinxdrupal_port', t('Port number must be positive.'));
  }
}
