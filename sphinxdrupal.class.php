<?php
/**
 * @file
 * The Sphinx Search module class
 */

if (!class_exists('SphinxClient')) {
  require_once drupal_get_path('module', 'sphinxdrupal') . '/lib/sphinxapi.php';
}

class SphinxDrupal {
  /**
   * SphinxClient instnace
   *
   * @var SphinxClient
   */
  public $sphinxsearch = NULL;

  /**
   * Simple constructor.
   *
   * @return SphinxDrupal
   */
  public function __construct() {
  }

  /**
   * Redirect to search page with specified query options.
   *
   * @param mixed $keywords
   *   This argument accepts a keywords string array or an already escaped
   *   keywords string.
   *
   * @return void
   *   Returns nothing
   */
  public static function gotoSearch($keywords = NULL) {
    // Key of array in the second parameter must be named as 'query'
    drupal_goto(self::getSearchPath(), array('query' => $keywords));
  }

  /**
   * Obtain the path to Sphinx search page.
   *
   * @return string
   *   Path to Sphinx search page
   */
  public static function getSearchPath() {
    return variable_get('sphinxdrupal_search_path', 'search-results');
  }

  /**
   * Check current path is search results path.
   *
   * @param string $path
   *   URL path string
   *
   * @return bool
   *   Return true if current path is search results path,
   *   otherwise return false
   */
  public static function isSearchPath($path = NULL) {
    if (!isset($path)) {
      $path = $_GET['q'];
    }
    return (strpos($path, self::getSearchPath()) === 0);
  }

  /**
   * Parses user request and makes options array.
   *
   * @param array $request_options
   *   Search options structure
   *
   * @return array
   *   Search results structure
   */
  public static function parseRequest($request_options = array()) {
    $search_options = array(
      'results_per_page' => 10,
      'filters' => array(),
      'errors' => array(),
      'excerpts_limit' => 500,
      'excerpts_around' => 10,
    );

    // Search keywords.
    if (isset($request_options['keywords'])) {
      $search_options['filters']['keywords']
        = trim($request_options['keywords']);
    }

    return $search_options;
  }

  /**
   * Returns cleaned query string.
   *
   * @param array $search_options
   *   Search options structure
   *
   * @return string
   *   Cleaned string
   */
  public static function getQueryString($search_options) {
    $keywords = array();

    // Search keywords.
    if (!empty($search_options['filters']['keywords'])) {
      $keywords['keywords'] = $search_options['filters']['keywords'];
    }

    return !empty($keywords) ?
      str_replace('%2C', ',', drupal_http_build_query($keywords)) :
      NULL;
  }

  /**
   * Execute a search keywords on the given options.
   *
   * @param array $search_options
   *   Search options structure
   *
   * @return array
   *   Search results structure
   */
  public function executeQuery($search_options) {
    $search_results = array(
      'error_message' => '',
      'warnings' => array(),
      'total_found' => 0,
      'total_available' => 0,
      'time' => 0,
      'words' => array(),
      'nodes' => array(),
      'titles' => array(),
      'excerpts' => array(),
    );
    $sphinx_query_keywords = $search_options['filters']['keywords'];

    // Obtain index name, required to resolve search keywords.
    $sphinxdrupal_index = variable_get('sphinxdrupal_index', 'drupal_search');
    if (empty($sphinxdrupal_index)) {
      $search_results['error_message'] = t('Sphinx index not specified.');
      return $search_results;
    }

    // Validate results per page option.
    if (!isset($search_options['results_per_page'])
      || $search_options['results_per_page'] <= 0
    ) {
      $search_options['results_per_page'] = 10;
    }

    // Quit if no search filter has been specified.
    if (empty($search_options['filters'])) {
      return $search_results;
    }

    // Prepare Sphinx client for search queries.
    $current_page = $this->getCurrentPage();
    $this->initSphinxClient();
    $this->sphinxsearch->SetMatchMode(SPH_MATCH_EXTENDED2);
    $this->sphinxsearch->ResetFilters();
    $this->sphinxsearch->ResetGroupBy();
    $this->sphinxsearch->SetLimits(
      $current_page * $search_options['results_per_page'],
      $search_options['results_per_page']
    );
    $this->sphinxsearch->SetFilter('status', array(0), TRUE);

    // Matching modes.
    $sphinx_query_keywords = trim($sphinx_query_keywords);

    // Send keywords to Sphinx.
    $sphinx_results = $this->sphinxsearch->Query($sphinx_query_keywords,
      $sphinxdrupal_index);
    if (!$sphinx_results) {
      $message = $this->sphinxsearch->GetLastError();
      $search_results['error_message']
        = t('Search failed using index %index. Sphinx error: %message', array(
          '%index' => $sphinxdrupal_index,
          '%message' => $message,
        ));
      return $search_results;
    }

    $message = $this->sphinxsearch->GetLastWarning();
    if (!empty($message)) {
      $search_results['warnings'][]
        = t('Search query warning: %message', array('%message' => $message));
    }
    if (empty($sphinx_results['matches'])) {
      return $search_results;
    }

    // Save Sphinx query results.
    $search_results['total_found'] = (int) $sphinx_results['total_found'];
    $search_results['total_available'] = (int) $sphinx_results['total'];
    $search_results['time'] = $sphinx_results['time'];
    $search_results['words']
      = isset($sphinx_results['words']) && is_array($sphinx_results['words']) ?
        $sphinx_results['words'] : array();

    // Load nodes referenced by returned results.
    foreach ($sphinx_results['matches'] as $sphinx_docid => $sphinx_match) {
      if (TRUE == ($node = node_load($sphinx_docid))) {
        $search_results['nodes'][] = $node;
        $search_results['titles'][] = check_plain($node->title);
        $search_results['excerpts'][] = $this->getNodeText(
          $node->body[LANGUAGE_NONE][0]['value']
        );
      }
    }

    // Use Sphinx to build excerpts.
    // Build node titles with highlighted keywords.
    $search_results['titles'] = $this->sphinxsearch->BuildExcerpts(
      $search_results['titles'],
      $sphinxdrupal_index, $sphinx_query_keywords,
      array(
        'before_match' => '<span class="search-keyword-match">',
        'after_match' => '</span>',
        'chunk_separator' => '',
        // We want all text here, so using a high enough number.
        'limit' => $search_options['excerpts_limit'],
        // Ignored when single_passage is TRUE.
        'around' => $search_options['excerpts_around'],
        'exact_phrase' => 0,
        'single_passage' => TRUE,
      )
    );
    if (!$search_results['titles']) {
      $search_results['titles'] = array();
      $search_results['warnings'][] = t(
        'Unable to build excerpts for content titles. Sphinx error: %message',
        array('%message' => $this->sphinxsearch->GetLastError()));
    }

    // Build node excerpts with highlighted keywords.
    $search_results['excerpts'] = $this->sphinxsearch->BuildExcerpts(
      $search_results['excerpts'],
      $sphinxdrupal_index, $sphinx_query_keywords,
      array(
        'before_match' => '<span class="search-keyword-match">',
        'after_match' => '</span>',
        'chunk_separator' => '<span class="search-chunk-separator"> ... </span>',
        'limit' => $search_options['excerpts_limit'],
        'around' => $search_options['excerpts_around'],
        'exact_phrase' => 0,
        'single_passage' => 0,
      )
    );
    if (!$search_results['excerpts']) {
      $search_results['excerpts'] = array();
      $search_results['warnings'][] = t(
        'Unable to build excerpts for content snippets. Sphinx error: %message',
        array('%message' => $this->sphinxsearch->GetLastError()));
    }

    return $search_results;
  }

  /**
   * Obtain current page from search results navigation.
   *
   * @param int $pager_element
   *   An optional integer to distinguish between multiple pagers on one page.
   *
   * @return int
   *   Current page number
   */
  public function getCurrentPage($pager_element = 0) {
    $pager_page_array = isset($_GET['page']) ?
      explode(',', $_GET['page']) :
      array();

    return
      isset($pager_page_array[$pager_element]) ?
        (int) $pager_page_array[$pager_element] :
        0;
  }

  /**
   * Instatiate a Sphinx search client object.
   *
   * @return void
   *   Returns nothing
   */
  public function initSphinxClient() {
    if (NULL == $this->sphinxsearch) {
      if (class_exists('SphinxClient')) {
        $this->sphinxsearch = new SphinxClient();
      }
      else {
        $this->sphinxsearch = new SphinxDrupalSphinxClient();
      }
      $this->sphinxsearch->SetServer(variable_get('sphinxdrupal_host', 'localhost'), (int) variable_get('sphinxdrupal_port', '3312'));
    }
  }

  /**
   * Creates pagination for search results.
   *
   * @param int $total_results
   *   Number of found results
   * @param int $results_per_page
   *   Number of results per each page
   * @param string $pager_element
   *   Name of a pager element
   *
   * @return string
   *   Returns nothing
   */
  public static function pager($total_results, $results_per_page, $pager_element = 0) {
    global $pager_page_array, $pager_total;

    $pager_total[$pager_element] = ceil($total_results / $results_per_page);
    if (!empty($_GET['page'])) {
      $pager_page_array = explode(',', $_GET['page']);
      $pager_page_array[$pager_element] = max(0, min((int) $pager_page_array[$pager_element], ((int) $pager_total[$pager_element]) - 1));
    }

    $variables = array(
      'tags' => array(),
      'element' => $pager_element,
      'parameters' => array(),
      'quantity'  => $total_results,
    );
    return theme('pager', $variables);
  }

  /**
   * Obtain the text representation of a node. All HTML is removed.
   *
   * @param string $text
   *   Source HTML text
   *
   * @return string
   *   Text representation of the node.
   */
  public function getNodeText($text) {
    // Strip control characters that aren't valid in XML.
    $text = $this->stripInvalidXMLCodes($text);

    // Strip off all tags, but insert space before/after them
    // to keep word boundaries.
    $text = str_replace(array('<', '>', '[', ']'),
      array(' <', '> ', ' ', ' '),
      $text);
    $text = preg_replace('#<(script|style)[^>]*>.*</\1>#s', ' ', $text);
    $text = strip_tags($text);

    // Reduce size a little removing redudant spaces and line breaks.
    $text = preg_replace("# +#", ' ', $text);
    $text = preg_replace("#(\s*)\n+#", "\n", $text);

    return $text;
  }

  /**
   * Strip control characters that aren't valid in XML.
   *
   * See http://www.w3.org/International/questions/qa-controls
   *
   * @return string
   *   Cleaned XML text
   */
  public function stripInvalidXMLCodes($text) {
    return preg_replace('#[\x00-\x08\x0B\x0C\x0E-\x1F]#S', ' ', $text);
  }
}
