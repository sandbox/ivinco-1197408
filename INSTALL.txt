Installation

Step 1 – Install Sphinx
Download(http://sphinxsearch.com/downloads/) Sphinx Search Engine.
Follow the installation instructions
(http://sphinxsearch.com/docs/current.html#installation).

Step 2 – Configure Sphinx
Download (https://bitbucket.org/ivinco/drupal-7-sphinx-search-module/downloads/
sphinxdrupal_latest.zip) and extract the module to a temporary directory.
This directory should not be web-accessible, so you should not use the modules
folder. In the module folder you can find sphinx.conf under etc directory.
Make sure to adjust all sphinx.conf values to suit your setup:
    * Set correct database, username, and password for your Drupal database
    * Update table names in SQL queries if your Drupal installation uses
      different prefix
    * Update the file paths (/www/drupal/sphinx/data/…, /www/drupal/sphinx…)
      and create folders as necessary


Step 3 – Run Sphinx indexer
Run the sphinx indexer to prepare for searching:
/path/to/sphinx/installation/bin/indexer --config /path/to/sphinx.conf --all

Step 4 – Test the Sphinx search
When the indexer is finished, test that sphinx searching is actually working:
/path/to/sphinx/installation/bin/search
   --config /path/to/sphinx.conf "test search phrase"

Step 5 – Start Sphinx daemon
/path/to/sphinx/installation/bin/searchd --config /path/to/sphinx.conf

In order to start Start Sphinx Search at boot, do the following:
    * write a script with following content:
      #!/usr/bin/bash
      /path/to/sphinx/installation/bin/searchd --config /path/to/sphinx.conf
    * put it in the /etc/init.d/ directory.
      Lets say you called it drupal_sphinx
    * make the file you created, drupal_sphinx, executable, using:
      chmod +x drupal_sphinx

In Debian based systems i.e. Ubuntu:
update-rc.d drupal_sphinx defaults

In Redhat based systems i.e. Fedora:
chkconfig --add drupal_sphinx

In windows based systems:
searchd.exe --install --config C:\Sphinx\sphinx.conf --servicename SphinxSearch

Step 6 – Setup scheduled jobs
To keep the index for the search engine up to date, the indexer must be
scheduled to run at a regular interval.
On most UNIX systems edit your crontab file by running the command:
crontab -e

Add this line to set up a cron job for the full index
– for example once every night:
0 3 * * * /path/to/sphinx/indexer --q --c /path/to/sphinx.conf drupal_search
  --rotate >/dev/null

Make sure to adjust the paths to suit your configuration.
Note that –rotate option is needed if searchd daemon is already running,
so that the indexer does not modify the index file while it is being used.
It creates a new file and copies it over the existing one when it is done.

Step 7 – Install Sphinx Search module
Sphinx Search is easily installed as any Drupal module:

    * Copy package contents to
      [drupal_document_root]/sites/all/modules/sphinxdrupal;
    * Goto Administer -> Modules (admin/modules) and enable the module;
    * Click on Permissions (admin/people/permissions#module-sphinxdrupal)
      to adjust permissions;
    * Click on Configure (admin/config/search/sphinxdrupal)
      to setup Sphinx connection parameters if necessary;
    * Goto Administer -> Structure -> Blocks (admin/structure/block)
      to setup Sphinx Search form on sidebar.

That's it!

Support
This module is developed by Ivinco (http://ivinco.com/).
If you need commercial support, or if you’d like Drupal Sphinx Search module
customized for your needs, we can help (http://ivinco.com/contact-us).
