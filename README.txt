Sphinx search module for Drupal 7.x

Drupal Sphinx search module enables the Sphinx-powered search
with high-performance and more relevant search results.

Features:
    * Google-like search syntax;
    * High searching performance;
    * Improved search result relevance;
    * Supports friendly URLs SEO settings;
    * Supports morphological word forms dictionaries;
    * Supports stemming (English, Russian and Czech are built-in);
    * Supports stopwords;
    * Supports both single-byte encodings and UTF-8;
    * And many other Sphinx features

Requirements:
    * Sphinx Search 0.9.9 or higher
    * Drupal 7.x
