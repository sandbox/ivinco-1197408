<?php
/**
 * @file
 * The view functions
 */

/**
 * Search page template function.
 *
 * @return string
 *   Rendered contents
 */
function sphinxdrupal_search_page() {
  if (isset($_POST['form_id'])) {
    // Let POST method redirect to GET.
    return drupal_render(drupal_get_form('sphinxdrupal_search_form'));
  }

  $sphinx_drupal = new SphinxDrupal();

  // Parse request and build search options structure.
  $search_options = SphinxDrupal::parseRequest($_GET);

  if (empty($search_options['filters'])) {
    return drupal_render(drupal_get_form('sphinxdrupal_search_form',
      $search_options));
  }

  // Execute search query and collect the results.
  $search_results = $sphinx_drupal->executeQuery($search_options);

  // If any, display warnings if user is administrator,
  // or store them to watchdog for later review.
  if (!empty($search_results['warnings'])) {
    sphinxdrupal_watchdog_warning(implode('<br />',
      $search_results['warnings']));
  }

  // Deal with search results.
  if ($search_results['total_available'] <= 0) {
    if (!empty($search_results['error_message'])) {
      drupal_set_message($search_results['error_message'], 'error');
    }

    // Display no results warning and render expanded search form.
    $output = '<h2>' . t('Your search yielded no results') . '</h2>';
    $output .= sphinxdrupal_help('sphinxdrupal#noresults', drupal_help_arg());
    $output .= drupal_render(drupal_get_form('sphinxdrupal_search_form',
      $search_options, FALSE));
  }
  else {
    // Render collapsed search form and search results.
    $output = drupal_render(drupal_get_form('sphinxdrupal_search_form',
      $search_options));
    $output .= '<h2>' . t('Search results') . '</h2>';
    $output .= sphinxdrupal_search_results(array(
        'search_options' => $search_options,
        'search_results' => $search_results,
    ));

    // Log the search request?
    /* @todo: logging requests
    $log_search_keys = variable_get('sphinxdrupal_log_search_keys', 'always');
    if ($log_search_keys == 'always' || ($log_search_keys == 'keys-only'
    && !empty($search_options['filters']['keywords']))
    ) {
    $query_string = SphinxDrupal::getQueryString($search_options);
    watchdog('search', '%keys %query', array(
    '%keys' => (!empty($search_options['filters']['keywords']) ?
    $search_options['filters']['keywords'] : t('(no keywords)')),
    '%query' => (!empty($query_string) ? '?' . urldecode($query_string) :
    ''),
    ), NULL,
    WATCHDOG_NOTICE,
    ''
    );
    }*/
  }

  return $output;
}

/**
 * Render a search box form.
 *
 * @return array
 *   Template structure
 */
function sphinxdrupal_search_box() {
  $form = array();
  // Build basic search box form.
  $form['inline'] = array(
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
  );
  $form['inline']['keywords'] = array(
    '#type' => 'textfield',
    '#size' => 15,
    '#default_value' => '',
    '#attributes' => array(
      'title' => t('Enter the terms you wish to search for.'),
    ),
  );

  $form['inline']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );

  return $form;
}

/**
 * Process a search box form submission.
 *
 * @param string $form
 *   Search form name
 * @param array $form_state
 *   Search form state structure
 *
 * @return void
 *   Returns nothing
 */
function sphinxdrupal_search_box_submit($form, &$form_state) {
  $query = array();
  $search_string = trim($form_state['values']['keywords']);
  if (!empty($search_string)) {
    $query['keywords'] = $search_string;
  }
  // Transform POST into a GET request.
  SphinxDrupal::gotoSearch($query);
}

/**
 * Renders search form.
 *
 * @param string $form
 *   Search form name
 * @param array $form_state
 *   Search form state structure
 * @param array $search_options
 *   Search options structure
 *
 * @return array
 *   Search form template structure
 */
function sphinxdrupal_search_form($form, &$form_state,
  $search_options = NULL
) {
  $form = array(
    '#action' => url(SphinxDrupal::getSearchPath()),
    '#attributes' => array('class' => 'search-form'),
  );
  $form['basic'] = array(
    '#type' => 'item',
    '#title' => t('Enter your keywords'),
  );
  $form['basic']['inline'] = array(
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
  );

  $default_value = !empty($search_options['filters']['keywords']) ?
    $search_options['filters']['keywords'] : '';
  $form['basic']['inline']['keywords'] = array(
    '#type' => 'textfield',
    '#title' => '',
    '#default_value' => $default_value,
    '#size' => 50,
    '#maxlength' => 255,
  );
  $form['basic']['inline']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );
  return $form;
}

/**
 * Validate a search form submission.
 *
 * @param string $form
 *   Search form name
 * @param array $form_state
 *   Search form state structure
 *
 * @return void
 *   Returns nothing
 */
function sphinxdrupal_search_form_validate($form, &$form_state) {
  $search_options = SphinxDrupal::parseRequest($form_state['values']);
  if (!empty($search_options['errors'])) {
    foreach ($search_options['errors'] as $field_name => $message) {
      form_set_error($field_name, $message);
    }
  }

}

/**
 * Process a search form submission.
 *
 * @param string $form
 *   Search form name
 * @param array $form_state
 *   Search form state structure
 *
 * @return void
 *   Returns nothing
 */
function sphinxdrupal_search_form_submit($form, &$form_state) {
  $search_options = SphinxDrupal::parseRequest($form_state['values']);
  $query_string = SphinxDrupal::getQueryString($search_options);
  if (empty($query_string)) {
    form_set_error('keys',
      t('Please enter some keywords and/or other search options.'));
  }
  // Transform POST into a GET request.
  $query = array();
  $search_string = trim($form_state['values']['keywords']);
  if (!empty($search_string)) {
    $query['keywords'] = $search_string;
  }

  SphinxDrupal::gotoSearch($query);
}

/**
 * Format the results page for the given query results array.
 *
 * @param array $variables
 *   Variables structure.
 *
 * @ingroup themeable
 *
 * @return string
 *   Rendered search results
 */
function sphinxdrupal_search_results($variables) {
  $search_results = array();
  $search_options = array();

  extract($variables);

  // Display information about query execution.
  $output = '<p>';
  $output .= format_plural($search_results['total_found'],
    '1 document found', '@count documents found') . ' ' .
    t('in @seconds seconds.',
    array('@seconds' => round($search_results['time'], 3)));

  if (!empty($search_results['words'])) {
    $words = array();
    foreach ($search_results['words'] as $word => $word_data) {
      $words[] = '<em>' . check_plain($word) . '</em> (' .
      t('documents: @docs, hits: @hits', array(
        '@docs' => (int) $word_data['docs'],
        '@hits' => (int) $word_data['hits'],
      )) . ')';
    }
    $output .= ' ' . t('Terms found:') . ' ' . implode('; ', $words) . '.';
  }
  $output .= '</p>' . "\n";
  if ($search_results['total_found'] != $search_results['total_available']) {
    drupal_set_message(
      t('Warning: only the first @count matches are displayed.',
      array('@count' => $search_results['total_available'])),
      'error'
    );
  }

  // Display list of formatted search results.
  $output .= '<dl class="search-results">' . "\n";
  foreach ($search_results['nodes'] as $item_id => $node) {
    $title = (isset($search_results['titles'][$item_id]) ?
      $search_results['titles'][$item_id] : check_plain($node->title));
    $output .= '<dt class="title">' .
      l($title, 'node/' . $node->nid, array('html' => TRUE)) . '</dt>' . "\n";

    $info = array(
      theme('username', array($node)),
      format_date($node->created, 'small'),
    );
    $extra = module_invoke_all('node_search node_result', $node);
    if (!empty($extra) && is_array($extra)) {
      $info = array_merge($info, $extra);
    }
    $excerpt = (isset($search_results['excerpts'][$item_id]) ?
      '<p class="search-excerpt">' . $search_results['excerpts'][$item_id] .
      '</p>' . "\n" : '');

    $output .= '<dd>' . $excerpt .
      '<p class="search-info">' . implode(' - ', $info) . '</p></dd>' . "\n";
  }
  $output .= '</dl>' . "\n";

  // Display pager.
  $output .= SphinxDrupal::pager($search_results['total_available'],
    $search_options['results_per_page']);

  return $output;
}

/**
 * Adds watchlog warning.
 *
 * @param string $message
 *   Text message
 *
 * @return void
 *   Returns nothing
 */
function sphinxdrupal_watchdog_warning($message) {
  if (user_access('administer sphinxdrupal')) {
    drupal_set_message($message, 'error');
  }
  else {
    watchdog('sphinxdrupal', $message, NULL, WATCHDOG_WARNING);
  }
}
