<?php
/**
 * @file
 * The Sphinx Search api class
 */

/**
 * Copyright (c) 2001-2008, Andrew Aksyonoff. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License. You should have
 * received a copy of the GPL license along with this program; if you
 * did not, you can find it at http://www.gnu.org/
 */

/**
 * PHP version of Sphinx searchd client (PHP API)
 */

// Known searchd commands.
define("SEARCHD_COMMAND_SEARCH", 0);
define("SEARCHD_COMMAND_EXCERPT", 1);
define("SEARCHD_COMMAND_UPDATE", 2);
define("SEARCHD_COMMAND_KEYWORDS", 3);
define("SEARCHD_COMMAND_PERSIST", 4);
define("SEARCHD_COMMAND_STATUS", 5);
define("SEARCHD_COMMAND_QUERY", 6);

// Current client-side command implementation versions.
define("VER_COMMAND_SEARCH", 0x116);
define("VER_COMMAND_EXCERPT", 0x100);
define("VER_COMMAND_UPDATE", 0x102);
define("VER_COMMAND_KEYWORDS", 0x100);
define("VER_COMMAND_STATUS", 0x100);
define("VER_COMMAND_QUERY", 0x100);

// Known searchd status codes.
define("SEARCHD_OK", 0);
define("SEARCHD_ERROR", 1);
define("SEARCHD_RETRY", 2);
define("SEARCHD_WARNING", 3);

// Known match modes.
define("SPH_MATCH_ALL", 0);
define("SPH_MATCH_ANY", 1);
define("SPH_MATCH_PHRASE", 2);
define("SPH_MATCH_BOOLEAN", 3);
define("SPH_MATCH_EXTENDED", 4);
define("SPH_MATCH_FULLSCAN", 5);
// Extended engine V2 (TEMPORARY, WILL BE REMOVED).
define("SPH_MATCH_EXTENDED2", 6);

/**
 *  Known ranking modes (ext2 only).
 */

// Default mode, phrase proximity major factor and BM25 minor one.
define("SPH_RANK_PROXIMITY_BM25", 0);
// Statistical mode, BM25 ranking only (faster but worse quality).
define("SPH_RANK_BM25", 1);
// No ranking, all matches get a weight of 1.
define("SPH_RANK_NONE", 2);
/**
 * Simple word-count weighting, rank is a weighted sum of per-field
 * keyword occurrence counts.
 */
define("SPH_RANK_WORDCOUNT", 3);
define("SPH_RANK_PROXIMITY", 4);
define("SPH_RANK_MATCHANY", 5);
define("SPH_RANK_FIELDMASK", 6);

// Known sort modes.
define("SPH_SORT_RELEVANCE", 0);
define("SPH_SORT_ATTR_DESC", 1);
define("SPH_SORT_ATTR_ASC", 2);
define("SPH_SORT_TIME_SEGMENTS", 3);
define("SPH_SORT_EXTENDED", 4);
define("SPH_SORT_EXPR", 5);

// Known filter types.
define("SPH_FILTER_VALUES", 0);
define("SPH_FILTER_RANGE", 1);
define("SPH_FILTER_FLOATRANGE", 2);

// Known attribute types.
define("SPH_ATTR_INTEGER", 1);
define("SPH_ATTR_TIMESTAMP", 2);
define("SPH_ATTR_ORDINAL", 3);
define("SPH_ATTR_BOOL", 4);
define("SPH_ATTR_FLOAT", 5);
define("SPH_ATTR_BIGINT", 6);
define("SPH_ATTR_MULTI", 0x40000000);

// Known grouping functions.
define("SPH_GROUPBY_DAY", 0);
define("SPH_GROUPBY_WEEK", 1);
define("SPH_GROUPBY_MONTH", 2);
define("SPH_GROUPBY_YEAR", 3);
define("SPH_GROUPBY_ATTR", 4);
define("SPH_GROUPBY_ATTRPAIR", 5);

/**
 * important properties of PHP's integers:
 * - always signed (one bit short of PHP_INT_SIZE)
 * - conversion from string to int is saturated
 * - float is double
 * - div converts arguments to floats
 * - mod converts arguments to ints
 *
 * the packing code below works as follows:
 * - when we got an int, just pack it
 * if performance is a problem, this is the branch users should aim for
 *
 * - otherwise, we got a number in string form
 * this might be due to different reasons, but we assume that this is
 * because it didn't fit into PHP int
 *
 * - factor the string into high and low ints for packing
 * - if we have bcmath, then it is used
 * - if we don't, we have to do it manually (this is the fun part)
 *
 * - x64 branch does factoring using ints
 * - x32 (ab)uses floats, since we can't fit unsigned 32-bit number into an int
 *
 * unpacking routines are pretty much the same.
 * - return ints if we can
 * - otherwise format number into a string
 */

/**
 * Pack 64-bit signed.
 *
 * @param int $v
 *   Integer value
 *
 * @return string
 *   Packed value
 */
function sphPackI64($v) {
  assert(is_numeric($v));

  // x64.
  if (PHP_INT_SIZE >= 8) {
    $v = (int) $v;
    return pack("NN", $v >> 32, $v & 0xFFFFFFFF);
  }

  // x32, int.
  if (is_int($v)) {
    return pack("NN", $v < 0 ? -1 : 0, $v);
  }

  // x32, bcmath.
  if (function_exists("bcmul")) {
    if (bccomp($v, 0) == -1) {
      $v = bcadd("18446744073709551616", $v);
    }
    $h = bcdiv($v, "4294967296", 0);
    $l = bcmod($v, "4294967296");

    // Conversion to float is intentional; int would lose 31st bit.
    return pack("NN", (float) $h, (float) $l);
  }

  // x32, no-bcmath.
  $p = max(0, strlen($v) - 13);
  $lo = abs((float) substr($v, $p));
  $hi = abs((float) substr($v, 0, $p));

  // (10 ^ 13) % (1 << 32) = 1316134912.
  $m = $lo + $hi * 1316134912.0;
  $q = floor($m / 4294967296.0);
  $l = $m - ($q * 4294967296.0);
  // (10 ^ 13) / (1 << 32) = 2328.
  $h = $hi * 2328.0 + $q;

  if ($v < 0) {
    if ($l == 0) {
      $h = 4294967296.0 - $h;
    }
    else {
      $h = 4294967295.0 - $h;
      $l = 4294967296.0 - $l;
    }
  }
  return pack("NN", $h, $l);
}

/**
 * Pack 64-bit unsigned.
 *
 * @param int $v
 *   Integer value
 *
 * @return string
 *   Packed value
 */
function sphPackU64($v) {
  assert(is_numeric($v));

  // x64.
  if (PHP_INT_SIZE >= 8) {
    assert($v >= 0);

    // x64, int.
    if (is_int($v)) {
      return pack("NN", $v >> 32, $v & 0xFFFFFFFF);
    }

    // x64, bcmath.
    if (function_exists("bcmul")) {
      $h = bcdiv($v, 4294967296, 0);
      $l = bcmod($v, 4294967296);
      return pack("NN", $h, $l);
    }

    // x64, no-bcmath.
    $p = max(0, strlen($v) - 13);
    $lo = (int) substr($v, $p);
    $hi = (int) substr($v, 0, $p);

    $m = $lo + $hi * 1316134912;
    $l = $m % 4294967296;
    $h = $hi * 2328 + (int) ($m / 4294967296);

    return pack("NN", $h, $l);
  }

  // x32, int.
  if (is_int($v)) {
    return pack("NN", 0, $v);
  }

  // x32, bcmath.
  if (function_exists("bcmul")) {
    $h = bcdiv($v, "4294967296", 0);
    $l = bcmod($v, "4294967296");

    // Conversion to float is intentional; int would lose 31st bit.
    return pack("NN", (float) $h, (float) $l);
  }

  // x32, no-bcmath.
  $p = max(0, strlen($v) - 13);
  $lo = (float) substr($v, $p);
  $hi = (float) substr($v, 0, $p);

  $m = $lo + $hi * 1316134912.0;
  $q = floor($m / 4294967296.0);
  $l = $m - ($q * 4294967296.0);
  $h = $hi * 2328.0 + $q;

  return pack("NN", $h, $l);
}

/**
 * Unpack 64-bit unsigned.
 *
 * @param string $v
 *   Packed value
 *
 * @return int
 *   Integer value
 */
function sphUnpackU64($v) {
  list($hi, $lo) = array_values(unpack("N*N*", $v));

  if (PHP_INT_SIZE >= 8) {
    // Because php 5.2.2 to 5.2.5 is totally fucked up again.
    if ($hi < 0) {
      $hi += (1 << 32);
    }
    if ($lo < 0) {
      $lo += (1 << 32);
    }

    // x64, int.
    if ($hi <= 2147483647) {
      return ($hi << 32) + $lo;
    }

    // x64, bcmath.
    if (function_exists("bcmul")) {
      return bcadd($lo, bcmul($hi, "4294967296"));
    }

    // x64, no-bcmath.
    $c = 100000;
    $h = ((int) ($hi / $c) << 32) + (int) ($lo / $c);
    $l = (($hi % $c) << 32) + ($lo % $c);
    if ($l > $c) {
      $h += (int) ($l / $c);
      $l = $l % $c;
    }

    if ($h == 0) {
      return $l;
    }
    return sprintf("%d%05d", $h, $l);
  }

  // x32, int.
  if ($hi == 0) {
    if ($lo > 0) {
      return $lo;
    }
    return sprintf("%u", $lo);
  }

  $hi = sprintf("%u", $hi);
  $lo = sprintf("%u", $lo);

  // x32, bcmath.
  if (function_exists("bcmul")) {
    return bcadd($lo, bcmul($hi, "4294967296"));
  }

  // x32, no-bcmath.
  $hi = (float) $hi;
  $lo = (float) $lo;

  $q = floor($hi / 10000000.0);
  $r = $hi - $q * 10000000.0;
  $m = $lo + $r * 4967296.0;
  $mq = floor($m / 10000000.0);
  $l = $m - $mq * 10000000.0;
  $h = $q * 4294967296.0 + $r * 429.0 + $mq;

  $h = sprintf("%.0f", $h);
  $l = sprintf("%07.0f", $l);
  if ($h == "0") {
    return sprintf("%.0f", (float) $l);
  }
  return $h . $l;
}

/**
 * Unpack 64-bit signed.
 *
 * @param string $v
 *   Packed value
 *
 * @return int
 *   Integer value
 */
function sphUnpackI64($v) {
  list($hi, $lo) = array_values(unpack("N*N*", $v));

  // x64.
  if (PHP_INT_SIZE >= 8) {
    // Because php 5.2.2 to 5.2.5 is totally fucked up again.
    if ($hi < 0) {
      $hi += (1 << 32);
    }
    if ($lo < 0) {
      $lo += (1 << 32);
    }

    return ($hi << 32) + $lo;
  }

  // x32, int.
  if ($hi == 0) {
    if ($lo > 0) {
      return $lo;
    }
    return sprintf("%u", $lo);

    // x32, int.
  }
  elseif ($hi == -1) {
    if ($lo < 0) {
      return $lo;
    }
    return sprintf("%.0f", $lo - 4294967296.0);
  }

  $neg = "";
  $c = 0;
  if ($hi < 0) {
    $hi = ~$hi;
    $lo = ~$lo;
    $c = 1;
    $neg = "-";
  }

  $hi = sprintf("%u", $hi);
  $lo = sprintf("%u", $lo);

  // x32, bcmath.
  if (function_exists("bcmul")) {
    return $neg . bcadd(bcadd($lo, bcmul($hi, "4294967296")), $c);
  }

  // x32, no-bcmath.
  $hi = (float) $hi;
  $lo = (float) $lo;

  $q = floor($hi / 10000000.0);
  $r = $hi - $q * 10000000.0;
  $m = $lo + $r * 4967296.0;
  $mq = floor($m / 10000000.0);
  $l = $m - $mq * 10000000.0 + $c;
  $h = $q * 4294967296.0 + $r * 429.0 + $mq;
  if ($l == 10000000) {
    $l = 0;
    $h += 1;
  }

  $h = sprintf("%.0f", $h);
  $l = sprintf("%07.0f", $l);
  if ($h == "0") {
    return $neg . sprintf("%.0f", (float) $l);
  }
  return $neg . $h . $l;
}

/**
 * Fixes uint value for Sphinx.
 *
 * @param int $value
 *   Source value
 *
 * @return int
 *   Fixed value
 */
function sphFixUint($value) {
  if (PHP_INT_SIZE >= 8) {
    // x64 route, workaround broken unpack() in 5.2.2+.
    if ($value < 0) {
      $value += (1 << 32);
    }

    return $value;
  }
  else {
    // x32 route, workaround php signed/unsigned braindamage.
    return sprintf("%u", $value);
  }
}

/**
 * Sphinx searchd client class.
 */
class SphinxDrupalSphinxClient {
  /**
   * Searchd host(default is "localhost")
   *
   * @var string
   */
  protected $host;

  /**
   * Searchd port(default is 9312)
   *
   * @var int
   */
  protected $port;

  /**
   * how many records to seek from result-set start(default is 0)
   *
   * @var int
   */
  protected $offset;

  /**
   * how many records to return from result-set starting at offset
   * (default is 20)
   *
   * @var int
   */
  protected $limit;

  /**
   * query matching mode(default is SPH_MATCH_ALL)
   *
   * @var int
   */
  protected $mode;

  /**
   * per-field weights(default is 1 for all fields)
   *
   * @var array
   */
  protected $weights;

  /**
   * match sorting mode(default is SPH_SORT_RELEVANCE)
   *
   * @var int
   */
  protected $sort;

  /**
   * attribute to sort by (defualt is "")
   *
   * @var string
   */
  protected $sortby;

  /**
   * min ID to match(default is 0, which means no limit)
   *
   * @var int
   */
  protected $minId;

  /**'
   * max ID to match(default is 0, which means no limit)
   *
   * @var int
   */
  protected $maxId;

  /**
   * Search filters
   *
   * @var array
   */
  protected $filters;

  /**
   * group-by attribute name
   *
   * @var string
   */
  protected $groupby;

  /**
   * group-by function(to pre-process group-by attribute value with)
   *
   * @var string
   */
  protected $groupfunc;

  /**
   * group-by sorting clause(to sort groups in result set with)
   *
   * @var string
   */
  protected $groupsort;

  /**
   * group-by count-distinct attribute
   *
   * @var string
   */
  protected $groupdistinct;

  /**
   * max matches to retrieve
   *
   * @var int
   */
  protected $maxmatches;

  /**
   * cutoff to stop searching at (default is 0)
   *
   * @var int
   */
  protected $cutoff;

  /**
   * distributed retries count
   *
   * @var int
   */
  protected $retrycount;

  /**
   * distributed retries delay
   *
   * @var int
   */
  protected $retrydelay;

  /**
   * geographical anchor point
   *
   * @var mixed
   */
  protected $anchor;

  /**
   * per-index weights
   *
   * @var array
   */
  protected $indexweights;

  /**
   * ranking mode(default is SPH_RANK_PROXIMITY_BM25)
   *
   * @var int
   */
  protected $ranker;

  /**
   * max query time, milliseconds(default is 0, do not limit)
   *
   * @var int
   */
  protected $maxquerytime;

  /**
   * per-field-name weights
   *
   * @var array
   */
  protected $fieldweights;

  /**
   * per-query attribute values overrides
   *
   * @var mixed
   */
  protected $overrides;

  /**
   * select-list(attributes or expressions, with optional aliases)
   *
   * @var string
   */
  protected $select;

  /**
   * last error message
   *
   * @var string
   */
  protected $error;

  /**
   * last warning message
   *
   * @var string
   */
  protected $warning;

  /**
   * connection error vs remote error flag
   *
   * @var string
   */
  protected $connerror;

  /**
   * requests array for multi-query
   *
   * @var array
   */
  protected $reqs;

  /**
   * stored mbstring encoding
   *
   * @var string
   */
  protected $mbenc;

  /**
   * whether $result["matches"] should be a hash or an array
   *
   * @var array
   */
  protected $arrayresult;

  /**
   * connect timeout
   *
   * @var int
   */
  protected $timeout;

  /**
   * Create a new client object and fill defaults.
   *
   * @return SphinxDrupalSphinxClient
   *   Created instance
   */
  public function __construct() {
    // Per-client-object settings.
    $this->host = "localhost";
    $this->port = 9312;
    $this->path = FALSE;
    $this->socket = FALSE;

    // Per-query settings.
    $this->offset = 0;
    $this->limit = 20;
    $this->mode = SPH_MATCH_ALL;
    $this->weights = array();
    $this->sort = SPH_SORT_RELEVANCE;
    $this->sortby = "";
    $this->minId = 0;
    $this->maxId = 0;
    $this->filters = array();
    $this->groupby = "";
    $this->groupfunc = SPH_GROUPBY_DAY;
    $this->groupsort = "@group desc";
    $this->groupdistinct = "";
    $this->maxmatches = 1000;
    $this->cutoff = 0;
    $this->retrycount = 0;
    $this->retrydelay = 0;
    $this->anchor = array();
    $this->indexweights = array();
    $this->ranker = SPH_RANK_PROXIMITY_BM25;
    $this->maxquerytime = 0;
    $this->fieldweights = array();
    $this->overrides = array();
    $this->select = "*";

    // Per-reply fields(for single-query case).
    $this->error = "";
    $this->warning = "";
    $this->connerror = FALSE;

    // Requests storage(for multi-query case).
    $this->reqs = array();
    $this->mbenc = "";
    $this->arrayresult = FALSE;
    $this->timeout = 0;
  }

  /**
   * Closes sockeck if required.
   *
   * @return void
   *   Nothing to return
   */
  public function __destruct() {
    if ($this->socket !== FALSE) {
      fclose($this->socket);
    }
  }

  /**
   * Get last error message(string).
   *
   * @return string
   *   Error string
   */
  public function GetLastError() {
    return $this->error;
  }

  /**
   * Get last warning message(string).
   *
   * @return string
   *   Warning string
   */
  public function GetLastWarning() {
    return $this->warning;
  }

  /**
   * Get last error flag.
   *
   * @return string
   *   Error string
   */
  public function IsConnectError() {
    return $this->connerror;
  }

  /**
   * Set searchd host name(string) and port(integer).
   *
   * @param string $host
   *   Server hostname or IP
   * @param int $port
   *   Serger port
   *
   * @return void
   *   Nothing to return
   */
  public function SetServer($host, $port = 0) {
    assert(is_string($host));
    if ($host[0] == '/') {
      $this->path = 'unix://' . $host;
      return;
    }
    if (substr($host, 0, 7) == "unix://") {
      $this->path = $host;
      return;
    }

    assert(is_int($port));
    $this->host = $host;
    $this->port = $port;
    $this->path = '';

  }

  /**
   * Set server connection timeout(0 to remove).
   *
   * @param int $timeout
   *   Connection timeout in seconds
   *
   * @return void
   *   Nothing to return
   */
  public function SetConnectTimeout($timeout) {
    assert(is_numeric($timeout));
    $this->timeout = $timeout;
  }

  /**
   * Send a package to the searchd server.
   *
   * @param socket $handle
   *   Socket handler
   * @param array $data
   *   Date to be sent
   * @param int $length
   *   Data size limit
   *
   * @return bool
   *   TRUE is success, FALSE on failure
   */
  protected function Send($handle, $data, $length) {
    if (feof($handle) || fwrite($handle, $data, $length) !== $length) {
      $this->error = 'connection unexpectedly closed(timed out?)';
      $this->connerror = TRUE;
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Enter mbstring workaround mode.
   *
   * @return void
   *   Nothing to return
   */
  protected function MBPush() {
    $this->mbenc = "";
    if (ini_get("mbstring.func_overload") & 2) {
      $this->mbenc = mb_internal_encoding();
      mb_internal_encoding("latin1");
    }
  }

  /**
   * Leave mbstring workaround mode.
   *
   * @return void
   *   Nothing to return
   */
  protected function MBPop() {
    if ($this->mbenc) {
      mb_internal_encoding($this->mbenc);
    }
  }

  /**
   * Connect to searchd server.
   *
   * @return socket
   *   Connection handler
   */
  protected function Connect() {
    if ($this->socket !== FALSE) {
      // We are in persistent connection mode, so we have a socket
      // however, need to check whether it's still alive.
      if (!@feof($this->socket)) {
        return $this->socket;
      }

      // Force reopen.
      $this->socket = FALSE;
    }

    $errno = 0;
    $errstr = "";
    $this->connerror = FALSE;

    if ($this->path) {
      $host = $this->path;
      $port = 0;
    }
    else {
      $host = $this->host;
      $port = $this->port;
    }

    if ($this->timeout <= 0) {
      $fp = @fsockopen($host, $port, $errno, $errstr);
    }
    else {
      $fp = @fsockopen($host, $port, $errno, $errstr, $this->timeout);
    }

    if (!$fp) {
      if ($this->path) {
        $location = $this->path;
      }
      else {
        $location = "{$this->host}:{$this->port}";
      }

      $errstr = trim($errstr);
      $this->error
        = "connection to $location failed(errno=$errno, msg=$errstr)";
      $this->connerror = TRUE;
      return FALSE;
    }

    // Send my version.
    if (!$this->Send($fp, pack("N", 1), 4)) {
      fclose($fp);
      $this->error = "failed to send client protocol version";
      return FALSE;
    }

    // Check version.
    list(, $v) = unpack("N*", fread($fp, 4));
    $v = (int) $v;
    if ($v < 1) {
      fclose($fp);
      $this->error = "expected searchd protocol version 1+, got version '$v'";
      return FALSE;
    }

    return $fp;
  }

  /**
   * Get and check response packet from searchd server.
   *
   * @param socket $fp
   *   Connection handler
   * @param int $client_ver
   *   Client version
   *
   * @return string
   *   Version comparsion string
   */
  protected function GetResponse($fp, $client_ver) {
    $response = "";
    $len = 0;

    $header = fread($fp, 8);
    if (strlen($header) == 8) {
      list($status, $ver, $len) = array_values(unpack("n2a/Nb", $header));
      $left = $len;
      while ($left > 0 && !feof($fp)) {
        $chunk = fread($fp, $left);
        if ($chunk) {
          $response .= $chunk;
          $left -= strlen($chunk);
        }
      }
    }

    if ($this->socket === FALSE) {
      fclose($fp);
    }

    // Check response.
    $read = strlen($response);
    if (!$response || $read != $len) {
      $this->error = $len ?
        "failed to read searchd response(status=$status, ver=$ver, "
          . "len=$len, read=$read)" :
        "received zero-sized searchd response";

      return FALSE;
    }

    // Check status.
    if ($status == SEARCHD_WARNING) {
      list(, $wlen) = unpack("N*", substr($response, 0, 4));
      $this->warning = substr($response, 4, $wlen);
      return substr($response, 4 + $wlen);
    }
    if ($status == SEARCHD_ERROR) {
      $this->error = "searchd error: " . substr($response, 4);
      return FALSE;
    }
    if ($status == SEARCHD_RETRY) {
      $this->error = "temporary searchd error: " . substr($response, 4);
      return FALSE;
    }
    if ($status != SEARCHD_OK) {
      $this->error = "unknown status code '$status'";
      return FALSE;
    }

    // Check version.
    if ($ver < $client_ver) {
      $this->warning = sprintf("searchd command v.%d.%d older than client's "
          . "v.%d.%d, some options might not work",
        $ver >> 8, $ver & 0xff, $client_ver >> 8, $client_ver & 0xff);
    }

    return $response;
  }

  /**
   * Set offset and count into result set.
   *
   * Optionally set max-matches and cutoff limits.
   *
   * @param int $offset
   *   Limit offset
   * @param int $limit
   *   Limit itself
   * @param int $max
   *   Max number of matches
   * @param int $cutoff
   *   Cutoff
   *
   * @return void
   *   Nothing to return
   */
  public function SetLimits($offset, $limit, $max = 0, $cutoff = 0) {
    assert(is_int($offset));
    assert(is_int($limit));
    assert($offset >= 0);
    assert($limit > 0);
    assert($max >= 0);
    $this->offset = $offset;
    $this->limit = $limit;
    if ($max > 0) {
      $this->maxmatches = $max;
    }

    if ($cutoff > 0) {
      $this->cutoff = $cutoff;
    }
  }

  /**
   * Set maximum query time, per-index integer, 0 means "do not limit".
   *
   * @param int $max
   *   Max query time in milliseconds
   *
   * @return void
   *   Nothing to return
   */
  public function SetMaxQueryTime($max) {
    assert(is_int($max));
    assert($max >= 0);
    $this->maxquerytime = $max;
  }

  /**
   * Set matching mode.
   *
   * @param int $mode
   *   Matching mode flags
   *
   * @return void
   *   Nothing to return
   */
  public function SetMatchMode($mode) {
    assert($mode == SPH_MATCH_ALL
      || $mode == SPH_MATCH_ANY
      || $mode == SPH_MATCH_PHRASE
      || $mode == SPH_MATCH_BOOLEAN
      || $mode == SPH_MATCH_EXTENDED
      || $mode == SPH_MATCH_FULLSCAN
      || $mode == SPH_MATCH_EXTENDED2);
    $this->mode = $mode;
  }

  /**
   * Set ranking mode.
   *
   * @param int $ranker
   *   Ranker mode flags
   *
   * @return void
   *   Nothing to return
   */
  public function SetRankingMode($ranker) {
    assert($ranker == SPH_RANK_PROXIMITY_BM25
      || $ranker == SPH_RANK_BM25
      || $ranker == SPH_RANK_NONE
      || $ranker == SPH_RANK_WORDCOUNT
      || $ranker == SPH_RANK_PROXIMITY
    );
    $this->ranker = $ranker;
  }

  /**
   * Set matches sorting mode.
   *
   * @param int $mode
   *   Sorting mode flags
   * @param string $sortby
   *   Sort field
   *
   * @return void
   *   Nothing to return
   */
  public function SetSortMode($mode, $sortby = "") {
    assert($mode == SPH_SORT_RELEVANCE
      || $mode == SPH_SORT_ATTR_DESC
      || $mode == SPH_SORT_ATTR_ASC
      || $mode == SPH_SORT_TIME_SEGMENTS
      || $mode == SPH_SORT_EXTENDED
      || $mode == SPH_SORT_EXPR);

    assert(is_string($sortby));
    assert($mode == SPH_SORT_RELEVANCE || strlen($sortby) > 0);

    $this->sort = $mode;
    $this->sortby = $sortby;
  }

  /**
   * Bind per-field weights by order.
   *
   * DEPRECATED; use SetFieldWeights() instead.
   *
   * @param array $weights
   *   Array of weights
   *
   * @return void
   *   Nothing to return
   */
  public function SetWeights($weights) {
    assert(is_array($weights));
    foreach ($weights as $weight) {
      assert(is_int($weight));
    }

    $this->weights = $weights;
  }

  /**
   * Bind per-field weights by name.
   *
   * @param array $weights
   *   Array of weights
   *
   * @return void
   *   Nothing to return
   */
  public function SetFieldWeights($weights) {
    assert(is_array($weights));
    foreach ($weights as $name => $weight) {
      assert(is_string($name));
      assert(is_int($weight));
    }
    $this->fieldweights = $weights;
  }

  /**
   * Bind per-index weights by name.
   *
   * @param array $weights
   *   Array of weights
   *
   * @return void
   *   Nothing to return
   */
  public function SetIndexWeights($weights) {
    assert(is_array($weights));
    foreach ($weights as $index => $weight) {
      assert(is_string($index));
      assert(is_int($weight));
    }
    $this->indexweights = $weights;
  }

  /**
   * Set IDs range to match.
   *
   * Only match records if document ID is beetwen $min and $max (inclusive).
   *
   * @param int $min
   *   Min value
   * @param int $max
   *   Max value
   *
   * @return void
   *   Nothing to return
   */
  public function SetIDRange($min, $max) {
    assert(is_numeric($min));
    assert(is_numeric($max));
    assert($min <= $max);

    $this->minId = $min;
    $this->maxId = $max;
  }

  /**
   * Set values set filter.
   *
   * Only match records where $attribute value is in given set.
   *
   * @param string $attribute
   *   Attribute name
   * @param array $values
   *   Array of attr values
   * @param bool $exclude
   *   FALSE means include
   *
   * @return void
   *   Nothing to return
   */
  public function SetFilter($attribute, $values, $exclude = FALSE) {
    assert(is_string($attribute));
    assert(is_array($values));
    assert(count($values));

    if (is_array($values) && count($values)) {
      foreach ($values as $value) {
        assert(is_numeric($value));
      }

      $this->filters[] = array(
        "type" => SPH_FILTER_VALUES,
        "attr" => $attribute,
        "exclude" => $exclude,
        "values" => $values,
      );
    }
  }

  /**
   * Set range filter.
   *
   * Only match records if $attribute value is
   * beetwen $min and $max (inclusive).
   *
   * @param string $attribute
   *   Attribute name
   * @param int $min
   *   Min value
   * @param int $max
   *   Max value
   * @param bool $exclude
   *   TRUE means include
   *
   * @return void
   *   Nothing to return
   */
  public function SetFilterRange($attribute, $min, $max, $exclude = FALSE) {
    assert(is_string($attribute));
    assert(is_numeric($min));
    assert(is_numeric($max));
    assert($min <= $max);

    $this->filters[] = array(
      "type" => SPH_FILTER_RANGE,
      "attr" => $attribute,
      "exclude" => $exclude,
      "min" => $min,
      "max" => $max,
    );
  }

  /**
   * Set float range filter.
   *
   * Only match records if $attribute value is
   * beetwen $min and $max (inclusive).
   *
   * @param string $attribute
   *   Attribute name
   * @param float $min
   *   Min float value
   * @param float $max
   *   Max float value
   * @param bool $exclude
   *   TRUE means include
   *
   * @return void
   *   Nothing to return
   */
  public function SetFilterFloatRange($attribute, $min, $max, $exclude = FALSE) {
    assert(is_string($attribute));
    assert(is_float($min));
    assert(is_float($max));
    assert($min <= $max);

    $this->filters[] = array(
      "type" => SPH_FILTER_FLOATRANGE,
      "attr" => $attribute,
      "exclude" => $exclude,
      "min" => $min,
      "max" => $max,
    );
  }

  /**
   * Setup anchor point for geosphere distance calculations.
   *
   * Required to use @geodist in filters and sorting
   * latitude and longitude must be in radians.
   *
   * @param string $attrlat
   *   Attribute for lat
   * @param string $attrlong
   *   Attribute for long
   * @param float $lat
   *   Lat value
   * @param float $long
   *   Long value
   *
   * @return void
   *   Nothing to return
   */
  public function SetGeoAnchor($attrlat, $attrlong, $lat, $long) {
    assert(is_string($attrlat));
    assert(is_string($attrlong));
    assert(is_float($lat));
    assert(is_float($long));

    $this->anchor = array(
      "attrlat" => $attrlat,
      "attrlong" => $attrlong,
      "lat" => $lat,
      "long" => $long,
    );
  }

  /**
   * Set grouping attribute and function.
   *
   * @param string $attribute
   *   Attribute name
   * @param string $func
   *   Groupping func
   * @param string $groupsort
   *   Groupping sort
   *
   * @return void
   *   Nothing to return
   */
  public function SetGroupBy($attribute, $func, $groupsort = "@group desc") {
    assert(is_string($attribute));
    assert(is_string($groupsort));
    assert($func == SPH_GROUPBY_DAY
      || $func == SPH_GROUPBY_WEEK
      || $func == SPH_GROUPBY_MONTH
      || $func == SPH_GROUPBY_YEAR
      || $func == SPH_GROUPBY_ATTR
      || $func == SPH_GROUPBY_ATTRPAIR);

    $this->groupby = $attribute;
    $this->groupfunc = $func;
    $this->groupsort = $groupsort;
  }

  /**
   * Set count-distinct attribute for group-by queries.
   *
   * @param string $attribute
   *   Attribute name
   *
   * @return void
   *   Nothing to return
   */
  public function SetGroupDistinct($attribute) {
    assert(is_string($attribute));
    $this->groupdistinct = $attribute;
  }

  /**
   * Set distributed retries count and delay.
   *
   * @param int $count
   *   Number of retries
   * @param int $delay
   *   Delay between delays
   *
   * @return void
   *   Nothing to return
   */
  public function SetRetries($count, $delay = 0) {
    assert(is_int($count) && $count >= 0);
    assert(is_int($delay) && $delay >= 0);
    $this->retrycount = $count;
    $this->retrydelay = $delay;
  }

  /**
   * Set result set format(hash or array; hash by default).
   *
   * PHP specific; needed for group-by-MVA result sets that may
   * contain duplicate IDs.
   *
   * @param array $arrayresult
   *   Result array
   *
   * @return void
   *   Nothing to return
   */
  public function SetArrayResult($arrayresult) {
    assert(is_bool($arrayresult));
    $this->arrayresult = $arrayresult;
  }

  /**
   * Set attribute values override.
   *
   * There can be only one override per attribute.
   * $values must be a hash that maps document IDs to attribute values.
   *
   * @param string $attrname
   *   Attribute name
   * @param arrat $attrtype
   *   Attribute types
   * @param array $values
   *   Value list
   *
   * @return void
   *   Nothing to return
   */
  public function SetOverride($attrname, $attrtype, $values) {
    assert(is_string($attrname));
    assert(in_array($attrtype, array(
      SPH_ATTR_INTEGER,
      SPH_ATTR_TIMESTAMP,
      SPH_ATTR_BOOL,
      SPH_ATTR_FLOAT,
      SPH_ATTR_BIGINT,
    )));
    assert(is_array($values));

    $this->overrides[$attrname] = array(
      "attr" => $attrname,
      "type" => $attrtype,
      "values" => $values,
    );
  }

  /**
   * Set select-list(attributes or expressions), SQL-like syntax.
   *
   * @param string $select
   *   Select part of the query
   *
   * @return void
   *   Nothing to return
   */
  public function SetSelect($select) {
    assert(is_string($select));
    $this->select = $select;
  }

  /**
   * Clear all filters(for multi-queries).
   *
   * @return void
   *   Nothing to return
   */
  public function ResetFilters() {
    $this->filters = array();
    $this->anchor = array();
  }

  /**
   * Clear groupby settings(for multi-queries).
   *
   * @return void
   *   Nothing to return
   */
  public function ResetGroupBy() {
    $this->groupby = "";
    $this->groupfunc = SPH_GROUPBY_DAY;
    $this->groupsort = "@group desc";
    $this->groupdistinct = "";
  }

  /**
   * Clear all attribute value overrides(for multi-queries).
   *
   * @return void
   *   Nothing to return
   */
  public function ResetOverrides() {
    $this->overrides = array();
  }

  /**
   * Connect to searchd server, run given search query through given indexes.
   *
   * @param string $query
   *   Search query
   * @param string $index
   *   Index name
   * @param string $comment
   *   Query Commnent
   *
   * @return array
   *   The search results
   */
  public function Query($query, $index = "*", $comment = "") {
    assert(empty($this->reqs));

    $this->AddQuery($query, $index, $comment);
    $results = $this->RunQueries();
    // Just in case it failed too early.
    $this->reqs = array();

    if (!is_array($results)) {
      // Probably network error; error message should be already filled.
      return FALSE;
    }

    $this->error = $results[0]["error"];
    $this->warning = $results[0]["warning"];
    if ($results[0]["status"] == SEARCHD_ERROR) {
      return FALSE;
    }
    else {
      return $results[0];
    }
  }

  /**
   * Helper to pack floats in network byte order.
   *
   * @param float $f
   *   Float value
   *
   * @return string
   *   Packed string
   */
  protected function PackFloat($f) {
    // Machine order.
    $t1 = pack("f", $f);
    // Note: int in machine order.
    list(, $t2) = unpack("L*", $t1);
    return pack("N", $t2);
  }

  /**
   * Add query to multi-query batch.
   *
   * @param string $query
   *   Query string
   * @param string $index
   *   Index name
   * @param string $comment
   *   Comment
   *
   * @return int
   *   index into results array from RunQueries() call
   */
  public function AddQuery($query, $index = "*", $comment = "") {
    // Note: mbstring workaround.
    $this->MBPush();

    // Build request: mode and limits.
    $req = pack("NNNNN", $this->offset, $this->limit,
      $this->mode, $this->ranker, $this->sort);
    $req .= pack("N", strlen($this->sortby)) . $this->sortby;

    // Query itself.
    $req .= pack("N", strlen($query)) . $query;
    // Weights.
    $req .= pack("N", count($this->weights));
    foreach ($this->weights as $weight) {
      $req .= pack("N", (int) $weight);
    }
    // Indexes.
    $req .= pack("N", strlen($index)) . $index;
    $req .= pack("N", 1);
    // id64 range.
    $req .= sphPackU64($this->minId) . sphPackU64($this->maxId);

    // Filters.
    $req .= pack("N", count($this->filters));
    foreach ($this->filters as $filter) {
      $req .= pack("N", strlen($filter["attr"])) . $filter["attr"];
      $req .= pack("N", $filter["type"]);
      switch ($filter["type"]) {
        case SPH_FILTER_VALUES:
          $req .= pack("N", count($filter["values"]));
          foreach ($filter["values"] as $value) {
            $req .= sphPackI64($value);
          }
          break;

        case SPH_FILTER_RANGE:
          $req .= sphPackI64($filter["min"]) . sphPackI64($filter["max"]);
          break;

        case SPH_FILTER_FLOATRANGE:
          $req .= $this->PackFloat($filter["min"])
            . $this->PackFloat($filter["max"]);
          break;

        default:
          assert(0 && "internal error: unhandled filter type");
      }
      $req .= pack("N", $filter["exclude"]);
    }

    // Group-by clause, max-matches count, group-sort clause, cutoff count.
    $req .= pack("NN", $this->groupfunc, strlen($this->groupby))
      . $this->groupby;
    $req .= pack("N", $this->maxmatches);
    $req .= pack("N", strlen($this->groupsort)) . $this->groupsort;
    $req .= pack("NNN", $this->cutoff, $this->retrycount,
      $this->retrydelay);
    $req .= pack("N", strlen($this->groupdistinct)) . $this->groupdistinct;

    // Anchor point.
    if (empty($this->anchor)) {
      $req .= pack("N", 0);
    }
    else {
      $a =& $this->anchor;
      $req .= pack("N", 1);
      $req .= pack("N", strlen($a["attrlat"])) . $a["attrlat"];
      $req .= pack("N", strlen($a["attrlong"])) . $a["attrlong"];
      $req .= $this->PackFloat($a["lat"]) . $this->PackFloat($a["long"]);
    }

    // Per-index weights.
    $req .= pack("N", count($this->indexweights));
    foreach ($this->indexweights as $idx => $weight) {
      $req .= pack("N", strlen($idx)) . $idx . pack("N", $weight);
    }

    // Max query time.
    $req .= pack("N", $this->maxquerytime);

    // Per-field weights.
    $req .= pack("N", count($this->fieldweights));
    foreach ($this->fieldweights as $field => $weight) {
      $req .= pack("N", strlen($field)) . $field . pack("N", $weight);
    }

    // Comment.
    $req .= pack("N", strlen($comment)) . $comment;

    // Attribute overrides.
    $req .= pack("N", count($this->overrides));
    foreach ($this->overrides as $entry) {
      $req .= pack("N", strlen($entry["attr"])) . $entry["attr"];
      $req .= pack("NN", $entry["type"], count($entry["values"]));
      foreach ($entry["values"] as $id => $val) {
        assert(is_numeric($id));
        assert(is_numeric($val));

        $req .= sphPackU64($id);
        switch ($entry["type"]) {
          case SPH_ATTR_FLOAT:
            $req .= $this->PackFloat($val);
            break;

          case SPH_ATTR_BIGINT:
            $req .= sphPackI64($val);
            break;

          default:
            $req .= pack("N", $val);
        }
      }
    }

    // Select-list.
    $req .= pack("N", strlen($this->select)) . $this->select;

    // Note: mbstring workaround.
    $this->MBPop();

    // Store request to requests array.
    $this->reqs[] = $req;
    return count($this->reqs) - 1;
  }

  /**
   * Connect to searchd, run queries batch.
   *
   * @return array
   *   An array of result sets
   */
  public function RunQueries() {
    if (empty($this->reqs)) {
      $this->error = "no queries defined, issue AddQuery() first";
      return FALSE;
    }

    // Note: mbstring workaround.
    $this->MBPush();

    if (!($fp = $this->Connect())) {
      $this->MBPop();
      return FALSE;
    }

    // Send query, get response.
    $nreqs = count($this->reqs);
    $req = implode("", $this->reqs);
    $len = 4 + strlen($req);
    // Add header.
    $req = pack("nnNN", SEARCHD_COMMAND_SEARCH, VER_COMMAND_SEARCH,
      $len, $nreqs) . $req;

    if (!($this->Send($fp, $req, $len + 8))
      || !($response = $this->GetResponse($fp, VER_COMMAND_SEARCH))
    ) {
      $this->MBPop();
      return FALSE;
    }

    // Query sent ok; we can reset reqs now.
    $this->reqs = array();

    // Parse and return response.
    return $this->ParseSearchResponse($response, $nreqs);
  }

  /**
   * Parse and return search query(or queries) response.
   *
   * @param string $response
   *   Response string
   * @param int $nreqs
   *   Number of requests
   *
   * @return array
   *   Array of parsed results
   */
  protected function ParseSearchResponse($response, $nreqs) {
    // Current position.
    $p = 0;
    // Max position for checks, to protect against broken responses.
    $max = strlen($response);

    $results = array();
    for ($ires = 0; $ires < $nreqs && $p < $max; $ires++) {
      $results[] = array();
      $result =& $results[$ires];

      $result["error"] = "";
      $result["warning"] = "";

      // Extract status.
      list(, $status) = unpack("N*", substr($response, $p, 4));
      $p += 4;
      $result["status"] = $status;
      if ($status != SEARCHD_OK) {
        list(, $len) = unpack("N*", substr($response, $p, 4));
        $p += 4;
        $message = substr($response, $p, $len);
        $p += $len;

        if ($status == SEARCHD_WARNING) {
          $result["warning"] = $message;
        }
        else {
          $result["error"] = $message;
          continue;
        }
      }

      // Read schema.
      $fields = array();
      $attrs = array();

      list(, $nfields) = unpack("N*", substr($response, $p, 4));
      $p += 4;
      while ($nfields-- > 0 && $p < $max) {
        list(, $len) = unpack("N*", substr($response, $p, 4));
        $p += 4;
        $fields[] = substr($response, $p, $len);
        $p += $len;
      }
      $result["fields"] = $fields;

      list(, $nattrs) = unpack("N*", substr($response, $p, 4));
      $p += 4;
      while ($nattrs-- > 0 && $p < $max) {
        list(, $len) = unpack("N*", substr($response, $p, 4));
        $p += 4;
        $attr = substr($response, $p, $len);
        $p += $len;
        list(, $type) = unpack("N*", substr($response, $p, 4));
        $p += 4;
        $attrs[$attr] = $type;
      }
      $result["attrs"] = $attrs;

      // Read match count.
      list(, $count) = unpack("N*", substr($response, $p, 4));
      $p += 4;
      list(, $id64) = unpack("N*", substr($response, $p, 4));
      $p += 4;

      // Read matches.
      $idx = -1;
      while ($count-- > 0 && $p < $max) {
        // Index into result array.
        $idx++;

        // Parse document id and weight.
        if ($id64) {
          $doc = sphUnpackU64(substr($response, $p, 8));
          $p += 8;
          list(, $weight) = unpack("N*", substr($response, $p, 4));
          $p += 4;
        }
        else {
          list($doc, $weight) = array_values(
            unpack("N*N*", substr($response, $p, 8))
          );
          $p += 8;
          $doc = sphFixUint($doc);
        }
        $weight = sprintf("%u", $weight);

        // Create match entry.
        if ($this->arrayresult) {
          $result["matches"][$idx] = array("id" => $doc, "weight" => $weight);
        }
        else {
          $result["matches"][$doc]["weight"] = $weight;
        }

        // Parse and create attributes.
        $attrvals = array();
        foreach ($attrs as $attr => $type) {
          // Handle 64bit ints.
          if ($type == SPH_ATTR_BIGINT) {
            $attrvals[$attr] = sphUnpackI64(substr($response, $p, 8));
            $p += 8;
            continue;
          }

          // Handle floats.
          if ($type == SPH_ATTR_FLOAT) {
            list(, $uval) = unpack("N*", substr($response, $p, 4));
            $p += 4;
            list(, $fval) = unpack("f*", pack("L", $uval));
            $attrvals[$attr] = $fval;
            continue;
          }

          // Handle everything else as unsigned ints.
          list(, $val) = unpack("N*", substr($response, $p, 4));
          $p += 4;
          if ($type & SPH_ATTR_MULTI) {
            $attrvals[$attr] = array();
            $nvalues = $val;
            while ($nvalues-- > 0 && $p < $max) {
              list(, $val) = unpack("N*", substr($response, $p, 4));
              $p += 4;
              $attrvals[$attr][] = sphFixUint($val);
            }
          }
          else {
            $attrvals[$attr] = sphFixUint($val);
          }
        }

        if ($this->arrayresult) {
          $result["matches"][$idx]["attrs"] = $attrvals;
        }
        else {
          $result["matches"][$doc]["attrs"] = $attrvals;
        }
      }

      list($total, $total_found, $msecs, $words)
        = array_values(unpack("N*N*N*N*", substr($response, $p, 16)));

      $result["total"] = sprintf("%u", $total);
      $result["total_found"] = sprintf("%u", $total_found);
      $result["time"] = sprintf("%.3f", $msecs / 1000);
      $p += 16;

      while ($words-- > 0 && $p < $max) {
        list(, $len) = unpack("N*", substr($response, $p, 4));
        $p += 4;
        $word = substr($response, $p, $len); $p += $len;
        list($docs, $hits) = array_values(unpack("N*N*",
          substr($response, $p, 8)));
        $p += 8;
        $result["words"][$word] = array(
          "docs" => sprintf("%u", $docs),
          "hits" => sprintf("%u", $hits),
        );
      }
    }

    $this->MBPop();
    return $results;
  }

  /**
   * Generate exceprts of given documents for given query.
   *
   * @param array $docs
   *   List of documents
   * @param string $index
   *   Index name
   * @param string $words
   *   Query words
   * @param array $opts
   *   Array of options
   *
   * @return bool
   *   FALSE on failure, an array of snippets on success
   */
  public function BuildExcerpts($docs, $index, $words, $opts = array()) {
    assert(is_array($docs));
    assert(is_string($index));
    assert(is_string($words));
    assert(is_array($opts));

    $this->MBPush();

    if (!($fp = $this->Connect())) {
      $this->MBPop();
      return FALSE;
    }

    // Fixup options.
    if (!isset($opts["before_match"])) {
      $opts["before_match"] = "<b>";
    }
    if (!isset($opts["after_match"])) {
      $opts["after_match"] = "</b>";
    }
    if (!isset($opts["chunk_separator"])) {
      $opts["chunk_separator"] = " ... ";
    }
    if (!isset($opts["limit"])) {
      $opts["limit"] = 256;
    }
    if (!isset($opts["around"])) {
      $opts["around"] = 5;
    }
    if (!isset($opts["exact_phrase"])) {
      $opts["exact_phrase"] = FALSE;
    }
    if (!isset($opts["single_passage"])) {
      $opts["single_passage"] = FALSE;
    }
    if (!isset($opts["use_boundaries"])) {
      $opts["use_boundaries"] = FALSE;
    }
    if (!isset($opts["weight_order"])) {
      $opts["weight_order"] = FALSE;
    }

    // Build request, v.1.0 req.
    $flags = 1;
    if ($opts["exact_phrase"]) {
      $flags |= 2;
    }
    if ($opts["single_passage"]) {
      $flags |= 4;
    }
    if ($opts["use_boundaries"]) {
      $flags |= 8;
    }
    if ($opts["weight_order"]) {
      $flags |= 16;
    }
    // Note: mode=0, flags=$flags.
    $req = pack("NN", 0, $flags);
    // Req index.
    $req .= pack("N", strlen($index)) . $index;
    // Req words.
    $req .= pack("N", strlen($words)) . $words;

    // Options.
    $req .= pack("N", strlen($opts["before_match"])) . $opts["before_match"];
    $req .= pack("N", strlen($opts["after_match"])) . $opts["after_match"];
    $req .= pack("N", strlen($opts["chunk_separator"]))
      . $opts["chunk_separator"];
    $req .= pack("N", (int) $opts["limit"]);
    $req .= pack("N", (int) $opts["around"]);

    // Documents.
    $req .= pack("N", count($docs));
    foreach ($docs as $doc) {
      assert(is_string($doc));
      $req .= pack("N", strlen($doc)) . $doc;
    }

    // Send query, get response.
    $len = strlen($req);
    // Add header.
    $req = pack("nnN", SEARCHD_COMMAND_EXCERPT, VER_COMMAND_EXCERPT, $len)
      . $req;
    if (!($this->Send($fp, $req, $len + 8))
      || !($response = $this->GetResponse($fp, VER_COMMAND_EXCERPT))
    ) {
      $this->MBPop();
      return FALSE;
    }

    // Parse response.
    $pos = 0;
    $res = array();
    $rlen = strlen($response);
    for ($i = 0; $i < count($docs); $i++) {
      list(, $len) = unpack("N*", substr($response, $pos, 4));
      $pos += 4;

      if ($pos + $len > $rlen) {
        $this->error = "incomplete reply";
        $this->MBPop();
        return FALSE;
      }
      $res[] = $len ? substr($response, $pos, $len) : "";
      $pos += $len;
    }

    $this->MBPop();
    return $res;
  }

  /**
   * Connect to searchd server, and generate keyword list for a given query.
   *
   * @param string $query
   *   Query string
   * @param string $index
   *   Index name
   * @param bool $hits
   *   Hits flag
   *
   * @return array
   *   An array of words on success and FALSE on failure
   */
  public function BuildKeywords($query, $index, $hits) {
    assert(is_string($query));
    assert(is_string($index));
    assert(is_bool($hits));

    $this->MBPush();

    if (!($fp = $this->Connect())) {
      $this->MBPop();
      return FALSE;
    }

    // Build request, .1.0 req, req query.
    $req = pack("N", strlen($query)) . $query;
    // Req index.
    $req .= pack("N", strlen($index)) . $index;
    $req .= pack("N", (int) $hits);

    // Send query, get response.
    $len = strlen($req);
    // Add header.
    $req = pack("nnN", SEARCHD_COMMAND_KEYWORDS, VER_COMMAND_KEYWORDS, $len)
      . $req;
    if (!($this->Send($fp, $req, $len + 8))
      || !($response = $this->GetResponse($fp, VER_COMMAND_KEYWORDS))
    ) {
      $this->MBPop();
      return FALSE;
    }

    // Parse response.
    $pos = 0;
    $res = array();
    $rlen = strlen($response);
    list(, $nwords) = unpack("N*", substr($response, $pos, 4));
    $pos += 4;
    for ($i = 0; $i < $nwords; $i++) {
      list(, $len) = unpack("N*", substr($response, $pos, 4));
      $pos += 4;
      $tokenized = $len ? substr($response, $pos, $len) : "";
      $pos += $len;

      list(, $len) = unpack("N*", substr($response, $pos, 4));
      $pos += 4;
      $normalized = $len ? substr($response, $pos, $len) : "";
      $pos += $len;

      $res[] = array("tokenized" => $tokenized, "normalized" => $normalized);

      if ($hits) {
        list($ndocs, $nhits)
          = array_values(unpack("N*N*", substr($response, $pos, 8)));
        $pos += 8;
        $res[$i]["docs"] = $ndocs;
        $res[$i]["hits"] = $nhits;
      }

      if ($pos > $rlen) {
        $this->error = "incomplete reply";
        $this->MBPop();
        return FALSE;
      }
    }

    $this->MBPop();
    return $res;
  }

  /**
   * Escapes string to be passed to sphinx.
   *
   * @param string $string
   *   Input string
   *
   * @return string
   *   Returns escaped string
   */
  public function EscapeString($string) {
    $from = array('\\', '(', ')', '|', '-', '!', '@',
      '~', '"', '&', '/', '^', '$', '=',
    );
    $to = array('\\\\', '\(', '\)', '\|', '\-', '\!', '\@',
      '\~', '\"', '\&', '\/', '\^', '\$', '\=',
    );

    return str_replace($from, $to, $string);
  }

  /**
   * Bbatch update given attributes in given rows in given indexes.
   *
   * @param string $index
   *   Index name
   * @param array $attrs
   *   Array of attributes
   * @param array $values
   *   Array of values
   * @param bool $mva
   *   Special param
   *
   * @return int
   *   Amount of updated documents(0 or more) on success, or -1 on failure
   */
  public function UpdateAttributes($index, $attrs, $values, $mva = FALSE) {
    // Verify everything.
    assert(is_string($index));
    assert(is_bool($mva));

    assert(is_array($attrs));
    foreach ($attrs as $attr) {
      assert(is_string($attr));
    }

    assert(is_array($values));
    foreach ($values as $id => $entry) {
      assert(is_numeric($id));
      assert(is_array($entry));
      assert(count($entry) == count($attrs));
      foreach ($entry as $v) {
        if ($mva) {
          assert(is_array($v));
          foreach ($v as $vv) {
            assert(is_int($vv));
          }
        }
        else {
          assert(is_int($v));
        }
      }
    }

    // Build request.
    $req = pack("N", strlen($index)) . $index;

    $req .= pack("N", count($attrs));
    foreach ($attrs as $attr) {
      $req .= pack("N", strlen($attr)) . $attr;
      $req .= pack("N", $mva ? 1 : 0);
    }

    $req .= pack("N", count($values));
    foreach ($values as $id => $entry) {
      $req .= sphPackU64($id);
      foreach ($entry as $v) {
        $req .= pack("N", $mva ? count($v) : $v);
        if ($mva) {
          foreach ($v as $vv) {
            $req .= pack("N", $vv);
          }
        }
      }
    }

    // Connect, send query, get response.
    if (!($fp = $this->Connect())) {
      return -1;
    }

    $len = strlen($req);
    // Add header.
    $req = pack("nnN", SEARCHD_COMMAND_UPDATE, VER_COMMAND_UPDATE, $len)
      . $req;
    if (!$this->Send($fp, $req, $len + 8)) {
      return -1;
    }

    if (!($response = $this->GetResponse($fp, VER_COMMAND_UPDATE))) {
      return -1;
    }

    // Parse response.
    list(, $updated) = unpack("N*", substr($response, 0, 4));
    return $updated;
  }

  /**
   * Creates persistent connections.
   *
   * @return bool
   *   Returns TRUE on success
   */
  public function Open() {
    if ($this->socket !== FALSE) {
      $this->error = 'already connected';
      return FALSE;
    }
    if (!$fp = $this->Connect()) {
      return FALSE;
    }

    // Command, command version = 0, body length = 4, body = 1.
    $req = pack("nnNN", SEARCHD_COMMAND_PERSIST, 0, 4, 1);
    if (!$this->Send($fp, $req, 12)) {
      return FALSE;
    }

    $this->socket = $fp;
    return TRUE;
  }

  /**
   * Close connection.
   *
   * @return bool
   *   Returns TRUE on success
   */
  public function Close() {
    if ($this->socket === FALSE) {
      $this->error = 'not connected';
      return FALSE;
    }

    fclose($this->socket);
    $this->socket = FALSE;

    return TRUE;
  }

  /**
   * Returns client status.
   *
   * @return string
   *   Status string
   */
  public function Status() {
    $this->MBPush();
    if (!($fp = $this->Connect())) {
      $this->MBPop();
      return FALSE;
    }

    // Note: len=4, body=1.
    $req = pack("nnNN", SEARCHD_COMMAND_STATUS, VER_COMMAND_STATUS, 4, 1);
    if (!($this->Send($fp, $req, 12))
      || !($response = $this->GetResponse($fp, VER_COMMAND_STATUS))
    ) {
      $this->MBPop();
      return FALSE;
    }

    // Just ignore length, error handling, etc.
    $res = substr($response, 4);
    $p = 0;
    list($rows, $cols)
      = array_values(unpack("N*N*", substr($response, $p, 8)));
    $p += 8;

    $res = array();
    for ($i = 0; $i < $rows; $i++) {
      for ($j = 0; $j < $cols; $j++) {
        list(, $len) = unpack("N*", substr($response, $p, 4));
        $p += 4;
        $res[$i][] = substr($response, $p, $len);
        $p += $len;
      }
    }

    $this->MBPop();
    return $res;
  }
}
